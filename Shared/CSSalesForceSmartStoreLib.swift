//
//  CSSalesForceSmartStoreLib.swift
//  CSSalesforceIntegrationLib
//
//  Created by Jonathan Constantino on 8/4/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import Foundation

class CSSalesForceSmartStoreLib {
	
	enum SoupQuerySpecType {
		case QuerySpecTypeAll
		case QuerySpecTypeExact
		case QuerySpecTypeLike
		case QuerySpecTypeMatch
		case QuerySpecTypeRange
		case QuerySpecTypeSQL
	}
	
	
	struct SoupQuerySpecStruct {
		var width = 0
		var height = 0
		//the kind of query, one of: "exact","range", or "like":
		//"exact" uses matchKey, "range" uses beginKey and endKey, "like" uses likeKey
		var queryType = "exact";
		
		//path for the original IndexSpec you wish to use for search: may be a compound path eg Account.Owner.Name
		var indexPath : String!
		
		//for queryType "exact"
		var matchKey : String!
		//for queryType "like"
		var likeKey : String!
		
		//for queryType "range"
		//the value at which query results may begin
		var beginKey : String!
		//the value at which query results may end
		var endKey : String!
		
		//"ascending" or "descending" : optional
		var order = SFSoupQuerySortOrder.Ascending;
		
		var orderPath : String!
		
		//the number of entries  per each cursor page
		var pageSize : UInt = 10;
		
		// eg "select count(*) from {employees}"
		var smartSql : String!
	}
	
	
	class func checkSoupExistAndRetrieveStore(kDefaultSmartStoreName : String, soupName : String)->(isExist: Bool, smartStore: SFSmartStore) {

		var exist = false
        var store = SFSmartStore.sharedStoreWithName(kDefaultSmartStoreName) as! SFSmartStore
		exist = store.soupExists(soupName)
		
		return (isExist: exist, smartStore: store)
		
	}
	
	class func registerSoup(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String, indexArray: [String]){
		
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
		
		var arrayofColumns = [[String:AnyObject]]()
		
		if (sfSmartStore.isExist == false) {
			for index in indexArray {
				var columnDict =  [String:AnyObject]()
					columnDict["path"] = index
					columnDict["type"] = "String"
				 arrayofColumns.append(columnDict)
			}
			sfSmartStore.smartStore.registerSoup(kDefaultSmartStoreSoupName, withIndexSpecs: arrayofColumns)
		}
	}
	
	class func removeSoup(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String) {
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
		
		if (sfSmartStore.isExist == true) {
			sfSmartStore.smartStore.removeSoup(kDefaultSmartStoreSoupName)
		}
	}
	
	class func removeSmartStoreDirectory(kDefaultSmartStoreName : String) {
		if let store: AnyObject! = SFSmartStore.sharedStoreWithName(kDefaultSmartStoreName){
			store.removeStoreDir(kDefaultSmartStoreName)
		}
	}
	

	class func upsertEntriesInSoup(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String, recordDict: [String:AnyObject]) -> [AnyObject]{
		var isSuccess : Bool =  false
		var arrayOfEntries = [AnyObject]()
		
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
	
		if (sfSmartStore.isExist == true) {
			var sfId = recordDict["sfid"] as! String
			var records = [AnyObject]()
			var error : NSErrorPointer!
			
			records.append(recordDict)
			arrayOfEntries = sfSmartStore.smartStore.upsertEntries(records, toSoup: kDefaultSmartStoreName, withExternalIdPath: sfId, error: error)
			
			if (error == nil) {
				println("Error Found: \(error)")
			}
		}
		return arrayOfEntries
	}
	
	class func removeEntriesInSoup(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String, entryIds: [String])
	{
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
		
		if (sfSmartStore.isExist == true) {
			sfSmartStore.smartStore.removeEntries(entryIds, fromSoup: kDefaultSmartStoreName)
		}
	}
	
	class func clearAllEntriesInSoup(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String){
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
		
		if (sfSmartStore.isExist == true) {
			sfSmartStore.smartStore.clearSoup(kDefaultSmartStoreSoupName)
			
		}
	}
	
	
	class func queryResult(smartStore: SFSmartStore, querySpec: SFQuerySpec!, index: UInt) -> [AnyObject]{
		var records = [AnyObject]()
		var error : NSErrorPointer!
		
		records = smartStore.queryWithQuerySpec(querySpec, pageIndex: index, error: error)
		
		if (error == nil) {
			println("Error Found: \(error)")
		}
		
		return records
	}
	
	
	class func refreshLocalData(kDefaultSmartStoreName : String, kDefaultSmartStoreSoupName : String!, soupQuerySpec : SoupQuerySpecStruct, soupQuerySpecType : SoupQuerySpecType) -> [AnyObject]{
		var queryRecords = [AnyObject]()
		var querySpec = SFQuerySpec()
		
		var sfSmartStore = checkSoupExistAndRetrieveStore(kDefaultSmartStoreName, soupName: kDefaultSmartStoreSoupName)
		
		if (sfSmartStore.isExist == false) {
			
		}
		
			switch soupQuerySpecType {
			case .QuerySpecTypeAll:
					 querySpec = SFQuerySpec.newAllQuerySpec(kDefaultSmartStoreSoupName, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
				
			case .QuerySpecTypeExact:
					 querySpec = SFQuerySpec.newExactQuerySpec(kDefaultSmartStoreSoupName, withPath: soupQuerySpec.indexPath, withMatchKey: soupQuerySpec.matchKey, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
				
			case .QuerySpecTypeLike:
					querySpec = SFQuerySpec.newLikeQuerySpec(kDefaultSmartStoreSoupName, withPath: soupQuerySpec.indexPath, withLikeKey: soupQuerySpec.likeKey, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
				
			case .QuerySpecTypeMatch:
					querySpec = SFQuerySpec.newMatchQuerySpec(kDefaultSmartStoreSoupName, withPath: soupQuerySpec.indexPath, withMatchKey: soupQuerySpec.matchKey, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
				
			case .QuerySpecTypeRange:
					querySpec = SFQuerySpec.newRangeQuerySpec(kDefaultSmartStoreSoupName, withPath: soupQuerySpec.indexPath, withBeginKey: soupQuerySpec.beginKey, withEndKey: soupQuerySpec.endKey, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
			
			case .QuerySpecTypeSQL:
					querySpec = SFQuerySpec.newSmartQuerySpec(soupQuerySpec.smartSql, withPageSize: soupQuerySpec.pageSize)
				
			default:
				querySpec = SFQuerySpec.newAllQuerySpec(kDefaultSmartStoreSoupName, withOrderPath: soupQuerySpec.orderPath, withOrder: soupQuerySpec.order, withPageSize: soupQuerySpec.pageSize)
			}
			
			queryRecords = queryResult(sfSmartStore.smartStore, querySpec: querySpec, index: soupQuerySpec.pageSize)
		
		return queryRecords
	}
	
	
	

}