//
//  SObjectDataFieldSpec.swift
//  CSSalesforceIntegrationLib
//
//  Created by Jonathan Constantino on 8/10/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import UIKit

class SObjectDataFieldSpec: NSObject {
	
	var fieldName : String?
	var isSearchable : Bool?
	
	init(fieldName: String, searchable: Bool){
		super.init()
		self.fieldName = fieldName
		self.isSearchable = searchable
	}
}
