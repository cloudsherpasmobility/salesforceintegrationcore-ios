//
//  SObjectDataSpec.swift
//  CSSalesforceIntegrationLib
//
//  Created by Jonathan Constantino on 8/10/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import UIKit

class SObjectDataSpec: NSObject {
	
	static let kSObjectIdField : String = "Id"

	
	let kSyncManagerLocal : String = "__local__"
    let kSoupIndexTypeString : String  = "string";

	
	var objectType : String?
	var objectFieldSpecs = [AnyObject]()
	var indexSpecs = [AnyObject]()
	var soupName : String?
	var orderByFieldName : String?
	
	var fieldNames:[String] {
		get {
			var mutableFieldNames = [String]()
			for fieldSpec in self.objectFieldSpecs {
				let fSpec = fieldSpec as! SObjectDataFieldSpec
				mutableFieldNames.append(fSpec.fieldName!)
			}
			return mutableFieldNames
		}
	}
	
	var soupFieldNames : [AnyObject]{
		get {
			var retNames = [AnyObject]()
			for fieldSpec in self.objectFieldSpecs {
				let fSpec = fieldSpec as! SObjectDataFieldSpec
				retNames.append(String(format:"{%@:%@}", self.soupName!, fSpec.fieldName!))
			}
			return retNames
		}
	}
	
    override init() {
        
    }
	
	
	init(objectType: String, objectFieldSpecs: [AnyObject], indexSpecs: [AnyObject], soupName: String, orderByFieldName: String){
		super.init()
		self.objectType = objectType;
		self.objectFieldSpecs = buildObjectFieldSpecs(objectFieldSpecs)
		self.indexSpecs = buildSoupIndexSpecs(indexSpecs)
		self.soupName = soupName
		self.orderByFieldName = orderByFieldName
	}
	
	// createSObjectData is abstract.
	// needs to be override.
	class func createSObjectData(soupDict: [String: AnyObject]) -> SObjectData {
		return SObjectData()
	}
	

	func buildObjectFieldSpecs(origObjectFieldSpecs: [AnyObject])->[AnyObject]{
		var foundIdFieldSpec : Bool = false
		for fieldSpec in origObjectFieldSpecs {
			let fSpec = fieldSpec as! SObjectDataFieldSpec
			let isEqual = (fSpec.fieldName == SObjectDataSpec.kSObjectIdField)
			if (isEqual == true) {
				foundIdFieldSpec = true
				break
			}
		}
		
		if (foundIdFieldSpec == false){
			var objectFieldSpecsWithId = origObjectFieldSpecs
			var idSpec = SObjectDataFieldSpec(fieldName: SObjectDataSpec.kSObjectIdField, searchable: false)
			objectFieldSpecsWithId.insert(idSpec, atIndex: 0)
			return objectFieldSpecsWithId
		}else{
			return origObjectFieldSpecs
		}
		
	}
	
	
	func buildSoupIndexSpecs(origIndexSpecs: [AnyObject])->[AnyObject]{
		var mutableIndexSpecs = origIndexSpecs
		var isLocalDataIndexSpec = SFSoupIndex(path: kSyncManagerLocal, indexType: kSoupIndexTypeString, columnName: kSyncManagerLocal)
		mutableIndexSpecs.insert(isLocalDataIndexSpec, atIndex: 0)
		
		var foundIdFieldSpec : Bool = false
		for indexSpec in origIndexSpecs {
			let indSpec = indexSpec as! SFSoupIndex
			let isEqual = (indSpec.path == SObjectDataSpec.kSObjectIdField)
			if (isEqual == true) {
				foundIdFieldSpec = true
				break
			}
		}
		
		if (foundIdFieldSpec == false){
			var idIndexSpec = SFSoupIndex(path: SObjectDataSpec.kSObjectIdField, indexType: kSoupIndexTypeString, columnName: SObjectDataSpec.kSObjectIdField)
			mutableIndexSpecs.insert(idIndexSpec, atIndex: 0)
		}
		
		return mutableIndexSpecs
	}
	
}
