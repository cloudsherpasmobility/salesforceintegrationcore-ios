//
//  SObjectDataManager.swift
//  CSSalesforceIntegrationLib
//
//  Created by Leo Quigao on 8/10/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import UIKit

class SObjectDataManager {
    
    let dataSpec: SObjectDataSpec
    let syncManager: SFSmartSyncSyncManager
    var syncDownId: Int = 0
    let kSyncLimit = 10000
    
    init(theDataSpec: SObjectDataSpec) {
        dataSpec = theDataSpec
        syncManager = SFSmartSyncSyncManager.sharedInstance(SFUserAccountManager.sharedInstance().currentUser)
    }
    
    func store() -> SFSmartStore {
        return SFSmartStore.sharedStoreWithName(kDefaultSmartStoreName) as! SFSmartStore
    }
    
    func refreshRemoteData() {
        if !store().soupExists(self.dataSpec.soupName) {
            self.registerSoup()
        }
        
        weak var weakSelf = self
        let updateBlock: SFSyncSyncManagerUpdateBlock = { sync in
            if sync.isDone() || sync.hasFailed() {
                weakSelf?.syncDownId = sync.syncId
                weakSelf?.refreshLocalData()
            }
        }
        
        if self.syncDownId == 0 {
            // first time
            let fieldNames = ",".join(self.dataSpec.fieldNames)
            let soqlQuery = "SELECT \(fieldNames), LastModifiedDate FROM \(self.dataSpec.objectType) LIMIT \(kSyncLimit)"
            let syncOptions = SFSyncOptions.newSyncOptionsForSyncDown(SFSyncStateMergeModeLeaveIfChanged)
            let syncTarget = SFSoqlSyncDownTarget.newSyncTarget(soqlQuery)
            self.syncManager.syncDownWithTarget(syncTarget, options: syncOptions, soupName: self.dataSpec.soupName, updateBlock: updateBlock)
        }
        else {
            // subsequent times
            self.syncManager.reSync(self.syncDownId, updateBlock: updateBlock)
        }
    }
    
    func updateRemoteData(updateBlock: SFSyncSyncManagerUpdateBlock) {
        let syncOptions = SFSyncOptions.newSyncOptionsForSyncUp(self.dataSpec.fieldNames, mergeMode: SFSyncStateMergeModeLeaveIfChanged)
        self.syncManager.syncUpWithOptions(syncOptions, soupName: self.dataSpec.soupName) { syncState in
            if syncState.isDone() || syncState.hasFailed() {
                updateBlock(syncState)
            }
        }
    }
    
    func registerSoup() {
        self.store().registerSoup(self.dataSpec.soupName, withIndexSpecs: self.dataSpec.indexSpecs)
    }
    
    func refreshLocalData() {
        // Already on nathan's code
    }
    
    func createLocalData(data: SObjectData) {
        data.updateSoupForFieldName(kSyncManagerLocal, fieldValue: true)
        data.updateSoupForFieldName(kSyncManagerLocallyCreated, fieldValue: true)
        self.store().upsertEntries([ data.soupDict ], toSoup: data.dynamicType.dataSpec().soupName)
    }
    
    func updateLocalData(data: SObjectData) {
        data.updateSoupForFieldName(kSyncManagerLocal, fieldValue: true)
        data.updateSoupForFieldName(kSyncManagerLocallyUpdated, fieldValue: true)
        self.store().upsertEntries([ data.soupDict ], toSoup: data.dynamicType.dataSpec().soupName, withExternalIdPath: SObjectDataSpec.kSObjectIdField, error: nil)
    }
    
    func deleteLocalData(data: SObjectData) {
        data.updateSoupForFieldName(kSyncManagerLocal, fieldValue: true)
        data.updateSoupForFieldName(kSyncManagerLocallyDeleted, fieldValue: true)
        self.store().upsertEntries([ data.soupDict ], toSoup: data.dynamicType.dataSpec().soupName, withExternalIdPath: SObjectDataSpec.kSObjectIdField, error: nil)
    }
    
    func dataHasLocalChanges(data: SObjectData) -> Bool {
        if let value = data.fieldValueForFieldName(kSyncManagerLocal) as? Bool {
            return value
        }
        return false
    }
    
    func dataLocallyCreated(data: SObjectData) -> Bool {
        if let value = data.fieldValueForFieldName(kSyncManagerLocallyCreated) as? Bool {
            return value
        }
        return false
    }
    
    func dataLocallyUpdate(data: SObjectData) -> Bool {
        if let value = data.fieldValueForFieldName(kSyncManagerLocallyUpdated) as? Bool {
            return value
        }
        return false
    }
    
    func dataLocallyDeleted(data: SObjectData) -> Bool {
        if let value = data.fieldValueForFieldName(kSyncManagerLocallyDeleted) as? Bool {
            return value
        }
        return false
    }
   
}
