//
//  SalesforceAccountManager.swift
//  CSSalesforceIntegrationLib
//
//  Created by David Joshua Dela Cruz on 8/6/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import Foundation

public protocol SalesforceAccountManagerDelegate {
	func loginSuccess()
	func loginFailed()
}

public class SalesforceAccountManager: NSObject {
	
	public var delegate: SalesforceIntegrationDelegate?
	
	static let kErrorField = "CSLoginManager.Error"
	enum LoginNotification : String
	{
		case LoggedIn = "LoggedIn"
		case LoginError = "LoginError"
		case LoggedOut = "LoggedOut"
		case SwitchUser = "SwitchedUser"
	}
	
	var consumerKey : String!
	var redirectURI : String!
	
	var lastLaunchAction : SFSDKLaunchAction = SFSDKLaunchActionNone
	
	func initializeSalesforce(remoteAccessConsumerKey: String, oAuthRedirectURI: String) {
		self.consumerKey = remoteAccessConsumerKey
		self.redirectURI = oAuthRedirectURI

		let sdkManager = SalesforceSDKManager.sharedManager()
		
		sdkManager.connectedAppId = self.consumerKey
		sdkManager.connectedAppCallbackUri = self.redirectURI
		sdkManager.authScopes = ["web", "api"]
		
		weak var weakSelf = self
		
		sdkManager.postLaunchAction = { launchActionList in
			self.lastLaunchAction = launchActionList
			if let myself = weakSelf
			{
				myself.postNotification(.LoggedIn, error:nil)
			}
		}
		
		sdkManager.launchErrorAction =
			{
				error, launchActionList in
				self.lastLaunchAction = launchActionList
				
				if let myself = weakSelf
				{
					myself.postNotification(.LoginError, error:error)
					SalesforceSDKManager.sharedManager().launch()
				}
		}
		sdkManager.postLogoutAction = {
			if let myself = weakSelf
			{
				myself.showLogin()
				myself.postNotification(.LoggedOut, error:nil)
			}
		}
		sdkManager.switchUserAction =
			{
				fromUser, toUser in
				
				if let myself = weakSelf
				{
					myself.postNotification(.SwitchUser, error:nil)
				}
		}
		
		SFAuthenticationManager.sharedManager().loginWithCompletion({ (authInfo:SFOAuthInfo!) -> Void in
				self.delegate?.loginSuccess()
			}, failure: { (authInfo: SFOAuthInfo!, error: NSError!) -> Void in
				self.delegate?.loginFailed()
		})
	}
	
	public func setDelegate(delegate: SalesforceIntegrationDelegate) {
		self.delegate = delegate
	}
	
	func postNotification(notification:LoginNotification, error:NSError?) {
		var userInfo = [String:AnyObject]()
		if let myError = error
		{
			userInfo[SalesforceAccountManager.kErrorField] = myError
		}
		
		NSNotificationCenter.defaultCenter().postNotificationName(notification.rawValue, object: self, userInfo:userInfo)
	}
	
	public func showLogin()	{
		let sdkManager = SalesforceSDKManager.sharedManager()
		sdkManager.launch()
	}

	public func logout() {
		SFAuthenticationManager.sharedManager().logout()
	}
	
}