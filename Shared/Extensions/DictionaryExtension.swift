//
//  DictionaryExtension.swift
//  CSSalesforceIntegrationLib
//
//  Created by David Joshua Dela Cruz on 8/10/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import Foundation

extension Dictionary {
	func nonNullObjectForKey(fieldName: String?) -> String? {
		return fieldName
	}
}