//
//  SalesforceIntegration.swift
//  CSSalesforceIntegrationLib
//
//  Created by Leo Quigao on 8/3/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

import Foundation

public protocol SalesforceIntegrationDelegate {
	func loginSuccess()
	func loginFailed()
}

@objc(SalesforceIntegration) public class SalesforceIntegration: NSObject, SFRestDelegate {
	
	public static let sharedInstance = SalesforceIntegration()

	public lazy var accountManager: SalesforceAccountManager = {
		SalesforceAccountManager()
	}()
	
	public func initializeSalesforce(remoteAccessConsumerKey: String, oAuthRedirectURI: String) {
		accountManager.initializeSalesforce(remoteAccessConsumerKey, oAuthRedirectURI: oAuthRedirectURI)
	}
}
