//
//  CoreBridgingHeader.h
//  SalesforceIntegrationCore
//
//  Created by Leo Quigao on 8/12/15.
//  Copyright (c) 2015 Cloud Sherpas. All rights reserved.
//

#ifndef SalesforceIntegrationCore_CoreBridgingHeader_h
#define SalesforceIntegrationCore_CoreBridgingHeader_h

#import <SalesforceRestAPI/SalesforceRestAPI.h>
#import <SmartSync/SmartSync.h>

#endif
